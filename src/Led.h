#ifndef __LED_H
#define __LED_H

#include "Arduino.h"
#include "Timer.h"

class Led {
  public:
    void Poll();
    void Toggle();
    Led(int pinNumber, double blinkFrequency);
  private:
    int ledPinNumber;
    Timer* timer;
};

#endif
