#define MAXLEDS 3
#include "Led.h"

double ledFrequencys[MAXLEDS] = {0.2, 3.0, 5.0};
int ledPins[MAXLEDS] = {13, 6, 7};
Led* leds[MAXLEDS];

void setup() {
  for(int i = 0; i < MAXLEDS; i++)
  {
    leds[i] = new Led(ledPins[i], ledFrequencys[i]);
  }
}

void loop() {
  for(int i = 0; i < MAXLEDS; i++)
  {
    leds[i]->Poll();
  }
}
