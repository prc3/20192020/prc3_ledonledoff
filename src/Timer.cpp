#include "Timer.h"

Timer::Timer(unsigned long frequencyTime) {
  elapseAt = frequencyTime;
  lastElapsed = millis();
}

bool Timer::TimeElapsed() {
  if(millis() - lastElapsed > elapseAt)
  {
    lastElapsed = millis();
    return true;
  }
  return false;
}

