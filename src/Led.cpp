#include "Led.h"

void Led::Poll() {
  if(timer != NULL) {
    if(timer->TimeElapsed())
    {
      Toggle();
    }
  }
}

void Led::Toggle() {
  digitalWrite(ledPinNumber, !digitalRead(ledPinNumber));
}

Led::Led(int pinNumber, double blinkFrequency) {
  long frequencyTime = blinkFrequency * 1000;
  timer = new Timer(frequencyTime);
  ledPinNumber = pinNumber;
  pinMode(pinNumber, OUTPUT);
}
