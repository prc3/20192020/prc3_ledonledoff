#ifndef __TIMER_H
#define __TIMER_H

#include "Arduino.h"

class Timer {
  public:
    bool TimeElapsed();
    Timer(unsigned long frequencyTime);

  private:
    unsigned long elapseAt, lastElapsed;
};

#endif
